#include"assemblerHeaders.h"



bool myPassOne::assemble(string path)
{
    ifstream inputFile ;
    ofstream passOneLog;
    expressionHandler ExprHandler(symbolTable);
    parser prsr ;
    string input;
    string out_path = "listingFile.txt" ;
    int locationCounter = 0;
    int lines = 0;
    int startingAddress;
    int initialLocctr = -1;
    symbolTable.clear();
    literalTable.clear();
    inputFile.open(path.c_str(), ios::in) ;
    outputFile.open(out_path.c_str(), ios::out) ;
    string logpass  = "passOneLog.txt" ;
    passOneLog.open(logpass.c_str(),ios::out);
    bool started=false,ended=false,successfulAssembly = true;
    if(!inputFile.is_open())
    {
        cout << "failed to open input file" << endl;
        return false;
    }
    while(!inputFile.eof())
    {
        getline (inputFile,input) ;
        prsr.handleLine(input);
        if(ended)
        {
            if(!prsr.isValid() || !prsr.isComment())
            {
                passOneLog << "****  " << formatNumber(lines,4)  <<  input << endl;
                passOneLog << "****  " << "assembly ended extra line" << endl;
                outputFile << "****  " << formatNumber(lines++,4)  <<  input << endl;
                outputFile << "****  " << "assembly ended extra line" << endl;
            }
            else
            {
                outputFile << formatNumber(lines++,4) <<  input << endl ;
            }
            continue;

        }

        if(!prsr.isValid())
        {
            passOneLog << "****  " << formatNumber(lines,4) << formatString("000000",15) << input << endl;
            passOneLog << "****  " <<  prsr.getError() << endl;
            outputFile << "****  " << formatNumber(lines++,4) << formatString("000000",15) << input << endl;
            outputFile << "****  " <<  prsr.getError() << endl;
            successfulAssembly = false;
        }
        else
        {
            if (prsr.isComment())
            {
                outputFile << formatNumber(lines++,4) <<  input << endl ;
            }
            else
            {
                if(!prsr.isDirective()) // an instruction
                {
                    if(!started)
                    {
                        started = true;
                        locationCounter = startingAddress =  0;
                    }

                    if(prsr.hasSymbol())
                    {
                        map<string,pair <int, string > >::iterator itr = symbolTable.find(prsr.getSymbol());
                        if(itr != symbolTable.end())
                        {
                            passOneLog << "**** " << formatNumber(lines,4)  << (formatString(prsr.hasSymbol()? prsr.getSymbol() : "",15)) << formatString(prsr.getInsOrDir(),15) << (formatString(prsr.getOperand(),15)) << endl;
                            passOneLog << "**** " << "duplicate Symbol " << prsr.getSymbol() << endl;
                            outputFile << "**** " << formatNumber(lines++,4)  << (formatString(prsr.hasSymbol()? prsr.getSymbol() : "",15)) << formatString(prsr.getInsOrDir(),15) << (formatString(prsr.getOperand(),15)) << endl;
                            outputFile << "**** " << "duplicate Symbol " << prsr.getSymbol() << endl;
                            successfulAssembly = false;
                            continue;
                        }
                        else
                        {
                            pair<int, string>p (locationCounter, "r") ;
                            symbolTable[prsr.getSymbol()] = p;
                        }
                    }
                    //handle literals right here
                    if(prsr.isLiteral())
                    {

                        if(!exists(prsr.getOperand(),literalTable))
                        {
                            literalTable[prsr.getOperand()] = -1;//add it with an unassigned address
                        }

                    }
                    outputFile << formatNumber(lines++,4)<< formatString(hexa(locationCounter,5),10) << (formatString(prsr.hasSymbol()? prsr.getSymbol() : "",15)) << formatString(prsr.getInsOrDir(),15) << (formatString(prsr.getOperand(),15)) << endl;
                    if(prsr.hasWarning())
                    {
                        outputFile << "****  " << prsr.getWarning() << endl;
                    }
                    locationCounter += prsr.getFormat();

                }
                //outputFile << formatNumber(lines++,4) << (formatString(prsr.hasSymbol()? prsr.getSymbol() : "",15)) << formatString(prsr.getInsOrDir(),15) << (formatString(prsr.getOperand(),15)) << endl;
                else  // a directive
                {
                    if(regex_match(prsr.getInsOrDir(),regex("^start$",regex_constants::icase)))
                    {
                        if(prsr.isExpression())
                        {
                            passOneLog << "****  " << formatNumber(lines,4)<<input << endl;
                            passOneLog << "****  " << "Invalid operand for the directive" << endl;
                            outputFile << "****  " << formatNumber(lines++,4)<<input << endl;
                            outputFile << "****  " << "Invalid operand for the directive" << endl;
                            successfulAssembly = false;
                            continue;
                        }
                        if(started)
                        {
                            passOneLog << "****  " << formatNumber(lines,4)<<input << endl;
                            passOneLog << "****  " << "late or repeated start directive" << endl;
                            outputFile << "****  " << formatNumber(lines++,4)<<input << endl;
                            outputFile << "****  " << "late or repeated start directive" << endl;
                            successfulAssembly = false;
                        }
                        else
                        {
                            int dummy = strtoul(prsr.getOperand().c_str(), NULL, 16);
                            if(dummy < 0 || dummy >=  1 << 20)
                            {
                                passOneLog << "****  " << formatNumber(lines,4)<<input << endl;
                                passOneLog << "****  " << "invalid start operand" << endl;
                                outputFile << "****  " << formatNumber(lines++,4)<<input << endl;
                                outputFile << "****  " << "invalid start operand" << endl;
                                successfulAssembly = false;
                                continue;
                            }

                            started = true;
                            startingAddress = locationCounter = strtoul(prsr.getOperand().c_str(), NULL, 16);
                            if(prsr.hasSymbol())
                            {
                                pair<int, string>p (locationCounter, "r") ;
                                symbolTable[prsr.getSymbol()] = p;
                            }
                            outputFile << formatNumber(lines++,4)<< formatString(hexa(locationCounter,5),10) << (formatString(prsr.hasSymbol()? prsr.getSymbol() : "",15)) << formatString(prsr.getInsOrDir(),15) << (formatString(prsr.getOperand(),15)) << endl;
                            if(prsr.hasWarning())
                            {
                                outputFile << "****  " << prsr.getWarning() << endl;
                            }
                        }

                    }
                    else if(regex_match(prsr.getInsOrDir(),regex("^end$",regex_constants::icase)))
                    {
                        if(prsr.isExpression())
                        {
                            passOneLog << "****  " << formatNumber(lines,4)<<input << endl;
                            passOneLog << "****  " << "Invalid operand for the directive" << endl;
                            outputFile << "****  " << formatNumber(lines++,4)<<input << endl;
                            outputFile << "****  " << "Invalid operand for the directive" << endl;
                            successfulAssembly = false;
                            continue;
                        }
                        if(prsr.hasSymbol())
                        {
                            map<string,pair<int, string>>::iterator itr = symbolTable.find(prsr.getSymbol());
                            if(itr != symbolTable.end())
                            {
                                outputFile << "**** " << formatNumber(lines++,4) << (formatString(prsr.hasSymbol()? prsr.getSymbol() : "",15)) << formatString(prsr.getInsOrDir(),15) << (formatString(prsr.getOperand(),15)) << endl;
                                outputFile << "**** " << "duplicate Symbol " << prsr.getSymbol() << endl;
                                successfulAssembly = false;
                                continue;
                            }
                            else
                            {
                                pair<int, string>p (locationCounter, "r") ;
                                symbolTable[prsr.getSymbol()] = p;
                            }
                        }
                        if(prsr.hasOperands())
                        {

                            map<string,pair<int, string>>::iterator itr = symbolTable.find(prsr.getOperand());
                            if(itr == symbolTable.end())
                            {
                                outputFile << "**** " << formatNumber(lines++,4) << (formatString(prsr.hasSymbol()? prsr.getSymbol() : "",15)) << formatString(prsr.getInsOrDir(),15) << (formatString(prsr.getOperand(),15)) << endl;
                                outputFile << "**** " << "operand not found in symbol table" << prsr.getSymbol() << endl;
                                successfulAssembly = false;
                                continue;
                            }
                            else //save the address of first executable instruction
                            {

                            }

                        }
                        ended = true;
                        outputFile << formatNumber(lines++,4)<< formatString(hexa(locationCounter,5),10) << (formatString(prsr.hasSymbol()? prsr.getSymbol() : "",15)) << formatString(prsr.getInsOrDir(),15) << (formatString(prsr.getOperand(),15)) << endl;
                        if(prsr.hasWarning())
                        {
                            outputFile << "****  " << prsr.getWarning() << endl;
                        }
                    }
                    else if(regex_match(prsr.getInsOrDir(),regex("^byte$",regex_constants::icase)))
                    {
                        if(prsr.isExpression())
                        {
                            passOneLog << "****  " << formatNumber(lines,4)<<input << endl;
                            passOneLog << "****  " << "Invalid operand for the directive" << endl;
                            outputFile << "****  " << formatNumber(lines++,4)<<input << endl;
                            outputFile << "****  " << "Invalid operand for the directive" << endl;
                            successfulAssembly = false;
                            continue;
                        }
                        int len = prsr.getOperand().length() - 3;
                        if(prsr.getOperand().at(0) == 'x' || prsr.getOperand().at(0) == 'X')
                        {
                            if(len%2 == 1) //odd
                            {
                                outputFile << "**** " << formatNumber(lines++,4) << (formatString(prsr.hasSymbol()? prsr.getSymbol() : "",15)) << formatString(prsr.getInsOrDir(),15) << (formatString(prsr.getOperand(),15)) << endl;
                                outputFile << "**** odd number of hexadecimal digits " << endl;
                            }
                        }

                        if(prsr.hasSymbol())
                        {
                            map<string,pair<int, string>>::iterator itr = symbolTable.find(prsr.getSymbol());
                            if(itr != symbolTable.end())
                            {
                                passOneLog << "**** " << formatNumber(lines,4) << (formatString(prsr.hasSymbol()? prsr.getSymbol() : "",15)) << formatString(prsr.getInsOrDir(),15) << (formatString(prsr.getOperand(),15)) << endl;
                                passOneLog << "**** " << "duplicate Symbol " << prsr.getSymbol() << endl;
                                outputFile << "**** " << formatNumber(lines++,4) << (formatString(prsr.hasSymbol()? prsr.getSymbol() : "",15)) << formatString(prsr.getInsOrDir(),15) << (formatString(prsr.getOperand(),15)) << endl;
                                outputFile << "**** " << "duplicate Symbol " << prsr.getSymbol() << endl;
                                successfulAssembly = false;
                                continue;
                            }
                            else
                            {
                                pair<int, string>p (locationCounter, "r") ;
                                symbolTable[prsr.getSymbol()] = p;
                            }
                        }

                        outputFile << formatNumber(lines++,4)<< formatString(hexa(locationCounter,5),10) << (formatString(prsr.hasSymbol()? prsr.getSymbol() : "",15)) << formatString(prsr.getInsOrDir(),15) << (formatString(prsr.getOperand(),15)) << endl;
                        if(prsr.hasWarning())
                        {
                            outputFile << "****  " << prsr.getWarning() << endl;
                        }

                        if(prsr.getOperand().at(0) == 'x' || prsr.getOperand().at(0) == 'X')
                        {
                            locationCounter += len/2;
                        }
                        else
                        {
                            locationCounter += len;
                        }



                    }
                    else  if(regex_match(prsr.getInsOrDir(),regex("^word$",regex_constants::icase)))
                    {

                        if(prsr.hasSymbol())
                        {
                            map<string,pair<int, string>>::iterator itr = symbolTable.find(prsr.getSymbol());
                            if(itr != symbolTable.end())
                            {
                                passOneLog << "**** " << formatNumber(lines,4) << (formatString(prsr.hasSymbol()? prsr.getSymbol() : "",15)) << formatString(prsr.getInsOrDir(),15) << (formatString(prsr.getOperand(),15)) << endl;
                                passOneLog << "**** " << "duplicate Symbol " << prsr.getSymbol() << endl;
                                outputFile << "**** " << formatNumber(lines++,4) << (formatString(prsr.hasSymbol()? prsr.getSymbol() : "",15)) << formatString(prsr.getInsOrDir(),15) << (formatString(prsr.getOperand(),15)) << endl;
                                outputFile << "**** " << "duplicate Symbol " << prsr.getSymbol() << endl;
                                successfulAssembly = false;
                                continue;
                            }
                            else
                            {
                                pair<int, string>p (locationCounter, "r") ;
                                symbolTable[prsr.getSymbol()] = p;
                            }
                        }

                        outputFile << formatNumber(lines++,4)<< formatString(hexa(locationCounter,5),10) << (formatString(prsr.hasSymbol()? prsr.getSymbol() : "",15)) << formatString(prsr.getInsOrDir(),15) << (formatString(prsr.getOperand(),15)) << endl;
                        if(prsr.hasWarning())
                        {
                            outputFile << "****  " << prsr.getWarning() << endl;
                        }
                        string number;
                        stringstream operandStream(prsr.getOperand());
                        while (getline(operandStream,number, ','))
                        {
                            locationCounter += 3 ;
                        }
                    }
                    else  if(regex_match(prsr.getInsOrDir(),regex("^resb$",regex_constants::icase)))
                    {
                        int a = -1 ;
                        if(prsr.isExpression())
                        {
                            ExprHandler.evaluate(prsr.getOperand(),symbolTable);
                            if(!ExprHandler.isValidExp())
                            {
                                passOneLog << "**** " << formatNumber(lines,4) << (formatString(prsr.hasSymbol()? prsr.getSymbol() : "",15)) << formatString(prsr.getInsOrDir(),15) << (formatString(prsr.getOperand(),15)) << endl;
                                passOneLog<< "**** " << "invalid directive operand " << prsr.getOperand() << endl;
                                outputFile << "**** " << formatNumber(lines++,4) << (formatString(prsr.hasSymbol()? prsr.getSymbol() : "",15)) << formatString(prsr.getInsOrDir(),15) << (formatString(prsr.getOperand(),15)) << endl;
                                outputFile << "**** " << "invalid directive operand " << prsr.getOperand() << endl;
                                successfulAssembly = false;
                                continue;
                            }
                            a = ExprHandler.getFinalResult();
                            if(a < 0  || a > 9999)
                            {
                                passOneLog << "**** " << formatNumber(lines,4) << (formatString(prsr.hasSymbol()? prsr.getSymbol() : "",15)) << formatString(prsr.getInsOrDir(),15) << (formatString(prsr.getOperand(),15)) << endl;
                                passOneLog<< "**** " << "invalid directive operand out of range 0 < operand < 9999 " << prsr.getOperand() << endl;
                                outputFile << "**** " << formatNumber(lines++,4) << (formatString(prsr.hasSymbol()? prsr.getSymbol() : "",15)) << formatString(prsr.getInsOrDir(),15) << (formatString(prsr.getOperand(),15)) << endl;
                                outputFile << "**** " << "invalid directive operand out of range 0 < operand < 9999 " << prsr.getOperand() << endl;
                                successfulAssembly = false;
                                continue;
                            }
                        }

                        if(prsr.hasSymbol())
                        {
                            map<string,pair<int, string>>::iterator itr = symbolTable.find(prsr.getSymbol());
                            if(itr != symbolTable.end())
                            {
                                passOneLog << "**** " << formatNumber(lines,4) << (formatString(prsr.hasSymbol()? prsr.getSymbol() : "",15)) << formatString(prsr.getInsOrDir(),15) << (formatString(prsr.getOperand(),15)) << endl;
                                passOneLog << "**** " << "duplicate Symbol " << prsr.getSymbol() << endl;
                                outputFile << "**** " << formatNumber(lines++,4) << (formatString(prsr.hasSymbol()? prsr.getSymbol() : "",15)) << formatString(prsr.getInsOrDir(),15) << (formatString(prsr.getOperand(),15)) << endl;
                                outputFile << "**** " << "duplicate Symbol " << prsr.getSymbol() << endl;
                                successfulAssembly = false;
                                continue;
                            }
                            else
                            {
                                pair<int, string>p (locationCounter, "r") ;
                                symbolTable[prsr.getSymbol()] = p;
                            }
                        }
                        outputFile << formatNumber(lines++,4)<< formatString(hexa(locationCounter,5),10) << (formatString(prsr.hasSymbol()? prsr.getSymbol() : "",15)) << formatString(prsr.getInsOrDir(),15) << (formatString(prsr.getOperand(),15)) << endl;
                        if(prsr.hasWarning())
                        {
                            outputFile << "****  " << prsr.getWarning() << endl;
                        }
                        if(!prsr.isExpression())
                        {
                            stringstream dummyStream;
                            dummyStream << prsr.getOperand();
                            int length;
                            dummyStream >> length;
                            locationCounter += length ;
                        }
                        else
                        {
                            locationCounter += a ;
                        }


                    }
                    else  if(regex_match(prsr.getInsOrDir(),regex("^resw$",regex_constants::icase)))
                    {
                        int a = -1 ;
                        if(prsr.isExpression())
                        {
                            ExprHandler.evaluate(prsr.getOperand(),symbolTable);
                            if(!ExprHandler.isValidExp())
                            {
                                passOneLog << "**** " << formatNumber(lines,4) << (formatString(prsr.hasSymbol()? prsr.getSymbol() : "",15)) << formatString(prsr.getInsOrDir(),15) << (formatString(prsr.getOperand(),15)) << endl;
                                passOneLog << "**** " << "invalid directive operand " << prsr.getOperand() << endl;
                                outputFile << "**** " << formatNumber(lines++,4) << (formatString(prsr.hasSymbol()? prsr.getSymbol() : "",15)) << formatString(prsr.getInsOrDir(),15) << (formatString(prsr.getOperand(),15)) << endl;
                                outputFile << "**** " << "invalid directive operand " << prsr.getOperand() << endl;
                                successfulAssembly = false;
                                continue;
                            }
                            a = ExprHandler.getFinalResult();
                            if(a < 0  || a > 9999)
                            {
                                passOneLog << "**** " << formatNumber(lines,4) << (formatString(prsr.hasSymbol()? prsr.getSymbol() : "",15)) << formatString(prsr.getInsOrDir(),15) << (formatString(prsr.getOperand(),15)) << endl;
                                passOneLog<< "**** " << "invalid directive operand out of range 0 < operand < 9999 " << prsr.getOperand() << endl;
                                outputFile << "**** " << formatNumber(lines++,4) << (formatString(prsr.hasSymbol()? prsr.getSymbol() : "",15)) << formatString(prsr.getInsOrDir(),15) << (formatString(prsr.getOperand(),15)) << endl;
                                outputFile << "**** " << "invalid directive operand out of range 0 < operand < 9999 " << prsr.getOperand() << endl;
                                successfulAssembly = false;
                                continue;
                            }
                        }
                        if(prsr.hasSymbol())
                        {
                            map<string,pair<int, string>>::iterator itr = symbolTable.find(prsr.getSymbol());
                            if(itr != symbolTable.end())
                            {
                                passOneLog << "**** " << formatNumber(lines,4) << (formatString(prsr.hasSymbol()? prsr.getSymbol() : "",15)) << formatString(prsr.getInsOrDir(),15) << (formatString(prsr.getOperand(),15)) << endl;
                                passOneLog << "**** " << "duplicate Symbol " << prsr.getSymbol() << endl;
                                outputFile << "**** " << formatNumber(lines++,4) << (formatString(prsr.hasSymbol()? prsr.getSymbol() : "",15)) << formatString(prsr.getInsOrDir(),15) << (formatString(prsr.getOperand(),15)) << endl;
                                outputFile << "**** " << "duplicate Symbol " << prsr.getSymbol() << endl;
                                successfulAssembly = false;
                                continue;
                            }
                            else
                            {
                                pair<int, string>p (locationCounter, "r") ;
                                symbolTable[prsr.getSymbol()] = p;
                            }
                        }

                        outputFile << formatNumber(lines++,4)<< formatString(hexa(locationCounter,5),10) << (formatString(prsr.hasSymbol()? prsr.getSymbol() : "",15)) << formatString(prsr.getInsOrDir(),15) << (formatString(prsr.getOperand(),15)) << endl;
                        if(prsr.hasWarning())
                        {
                            outputFile << "****  " << prsr.getWarning() << endl;
                        }
                        if(!prsr.isExpression())
                        {
                            stringstream dummyStream;
                            dummyStream << prsr.getOperand();
                            int length;
                            dummyStream >> length;
                            locationCounter += 3 *length ;
                        }
                        else
                        {
                            locationCounter += 3*a;
                        }

                    }
                    else  if(regex_match(prsr.getInsOrDir(),regex("^org$",regex_constants::icase)))
                    {
                        if(prsr.hasOperands()) // has value
                        {
                            ExprHandler.evaluate(prsr.getOperand(),symbolTable);
                            if(ExprHandler.isValidExp())
                            {
                                if(ExprHandler.getFinalResult() < 0 || ExprHandler.getFinalResult() > (1 << 20))
                                {
                                    passOneLog << "**** " << formatNumber(lines,4) << (formatString(prsr.hasSymbol()? prsr.getSymbol() : "",15)) << formatString(prsr.getInsOrDir(),15) << (formatString(prsr.getOperand(),15)) << endl;
                                    passOneLog << "**** " << "location counter can't take a -ve value  " << prsr.getSymbol() << endl;
                                    outputFile << "**** " << formatNumber(lines++,4) << (formatString(prsr.hasSymbol()? prsr.getSymbol() : "",15)) << formatString(prsr.getInsOrDir(),15) << (formatString(prsr.getOperand(),15)) << endl;
                                    outputFile << "**** " << "location counter can't take a -ve value  " << prsr.getSymbol() << endl;
                                    successfulAssembly = false;
                                    continue;
                                }
                                initialLocctr = locationCounter;
                                locationCounter = ExprHandler.getFinalResult();
                                outputFile << formatNumber(lines++,4)<< formatString(hexa(locationCounter,5),10) << (formatString(prsr.hasSymbol()? prsr.getSymbol() : "",15)) << formatString(prsr.getInsOrDir(),15) << (formatString(prsr.getOperand(),15)) << endl;
                            }
                            else
                            {
                                passOneLog << "**** " << formatNumber(lines,4) << (formatString(prsr.hasSymbol()? prsr.getSymbol() : "",15)) << formatString(prsr.getInsOrDir(),15) << (formatString(prsr.getOperand(),15)) << endl;
                                passOneLog << "**** " << "illegal usage for ORG ,Undifined Symbol" << endl;
                                outputFile << "**** " << formatNumber(lines++,4) << (formatString(prsr.hasSymbol()? prsr.getSymbol() : "",15)) << formatString(prsr.getInsOrDir(),15) << (formatString(prsr.getOperand(),15)) << endl;
                                outputFile << "**** " << "illegal usage for ORG ,Undifined Symbol" << endl;
                                successfulAssembly = false;
                                continue;
                            }
                        }
                        else  // reset the previous value
                        {
                            if(initialLocctr != -1)
                            {
                                locationCounter = initialLocctr;
                            }
                            outputFile << formatNumber(lines++,4)<< formatString(hexa(locationCounter,5),10) << (formatString(prsr.hasSymbol()? prsr.getSymbol() : "",15)) << formatString(prsr.getInsOrDir(),15) << (formatString(prsr.getOperand(),15)) << endl;
                        }

                    }
                    else  if(regex_match(prsr.getInsOrDir(),regex("^(base|nobase)$",regex_constants::icase)))
                    {
                        if(prsr.hasSymbol())
                        {
                            map<string,pair<int, string>>::iterator itr = symbolTable.find(prsr.getSymbol());
                            if(itr != symbolTable.end())
                            {
                                passOneLog << "**** " << formatNumber(lines,4) << (formatString(prsr.hasSymbol()? prsr.getSymbol() : "",15)) << formatString(prsr.getInsOrDir(),15) << (formatString(prsr.getOperand(),15)) << endl;
                                passOneLog << "**** " << "duplicate Symbol " << prsr.getSymbol() << endl;
                                outputFile << "**** " << formatNumber(lines++,4) << (formatString(prsr.hasSymbol()? prsr.getSymbol() : "",15)) << formatString(prsr.getInsOrDir(),15) << (formatString(prsr.getOperand(),15)) << endl;
                                outputFile << "**** " << "duplicate Symbol " << prsr.getSymbol() << endl;
                                successfulAssembly = false;
                                continue;
                            }
                            else
                            {
                                pair<int, string>p (locationCounter, "r") ;
                                symbolTable[prsr.getSymbol()] = p;
                            }
                        }

                        outputFile << formatNumber(lines++,4)<< formatString(hexa(locationCounter,5),10) << (formatString(prsr.hasSymbol()? prsr.getSymbol() : "",15)) << formatString(prsr.getInsOrDir(),15) << (formatString(prsr.getOperand(),15)) << endl;
                        if(prsr.hasWarning())
                        {
                            outputFile << "****  " << prsr.getWarning() << endl;
                        }
                    }
                    else  if(regex_match(prsr.getInsOrDir(),regex("^equ$",regex_constants::icase)))
                    {
                        ExprHandler.evaluate(prsr.getOperand(),symbolTable);
                        if(!ExprHandler.isValidExp())
                        {
                            passOneLog << "****  " << formatNumber(lines,4)<<input << endl;
                            passOneLog << "****  " << "Invalid operand for the directive" << endl;
                            outputFile << "****  " << formatNumber(lines++,4)<<input << endl;
                            outputFile << "****  " << "Invalid operand for the directive" << endl;
                            successfulAssembly = false;
                            continue;
                        }

                        map<string,pair <int, string >>::iterator itr = symbolTable.find(prsr.getSymbol());
                        if(itr == symbolTable.end())
                        {
                            pair<int, string>p (ExprHandler.getFinalResult(), ExprHandler.isRelocatable()) ;
                            symbolTable[prsr.getSymbol()] = p;
                            outputFile << formatNumber(lines++,4)<< formatString(hexa(locationCounter,5),10) << (formatString(prsr.hasSymbol()? prsr.getSymbol() : "",15)) << formatString(prsr.getInsOrDir(),15) << (formatString(prsr.getOperand(),15)) << endl;
                        }
                        else
                        {

                            passOneLog << "**** " << formatNumber(lines,4) << (formatString(prsr.hasSymbol()? prsr.getSymbol() : "",15)) << formatString(prsr.getInsOrDir(),15) << (formatString(prsr.getOperand(),15)) << endl;
                            passOneLog << "**** " << "duplicate Symbol " << prsr.getSymbol() << endl;
                            outputFile << "**** " << formatNumber(lines++,4) << (formatString(prsr.hasSymbol()? prsr.getSymbol() : "",15)) << formatString(prsr.getInsOrDir(),15) << (formatString(prsr.getOperand(),15)) << endl;
                            outputFile << "**** " << "duplicate Symbol " << prsr.getSymbol() << endl;
                            successfulAssembly = false;
                            continue;
                        }

                    }
                    else  if(regex_match(prsr.getInsOrDir(),regex("^ltorg$",regex_constants::icase)))
                    {
                        if(prsr.isExpression())
                        {
                            passOneLog << "****  " << formatNumber(lines,4)<<input << endl;
                            passOneLog << "****  " << "Invalid operand for the directive" << endl;
                            outputFile << "****  " << formatNumber(lines++,4)<<input << endl;
                            outputFile << "****  " << "Invalid operand for the directive" << endl;
                            successfulAssembly = false;
                            continue;
                        }

                        for (map<string,int>::iterator it=literalTable.begin(); it!=literalTable.end(); ++it)
                        {
                            if(it->second == -1) //not set
                            {
                                literalTable[it->first] = locationCounter;
                                outputFile << formatNumber(lines,4)<< formatString(hexa(locationCounter,5),10) << (formatString(it->first,15))  << endl;
                                locationCounter += getLiteralLength(it->first);
                            }
                        }


                    }
                }
            }

        }
    }
    for (map<string,int>::iterator it=literalTable.begin(); it!=literalTable.end(); ++it)
    {
        if(it->second == -1) //not set
        {
            literalTable[it->first] = locationCounter;
            outputFile << formatNumber(lines,4)<< formatString(hexa(locationCounter,5),10) << (formatString(it->first,15))  << endl;
            locationCounter += getLiteralLength(it->first);
        }
    }
    if(!started)
    {
        outputFile << "**** malformed source file, no valid statements exist!!" << endl;
        successfulAssembly = false;
    }
    outputFile << "End.";
    if(successfulAssembly)
    {
        outputFile << endl << endl << "Successful Assembly!!" << endl;
    }
    else
    {
        outputFile << endl << endl <<  "Incomplete Assembly" << endl;
    }
    length = locationCounter - startingAddress;
    printTable(symbolTable,"Symbol Table");
    outputFile << endl << endl ;
    printLitTable(literalTable,"Literal Table");
    return successfulAssembly;

}


string myPassOne::formatString (string line,int length)
{
    int l= length - line.length();
    string ret = "";
    ret.append(line);
    if(line.length() < (unsigned)length)
        ret.append(l,' ');
    return ret;
}





string myPassOne::formatNumber(int number,int length)
{
    stringstream ss;
    ss << number;
    string str = ss.str();
    if (str.length() < (unsigned)length)
        str.append(length - str.length(),' ');
    return str;

}





void myPassOne::printTable(map<string, pair<int, string> > table,string msg)
{
    outputFile << endl << endl << formatString(msg,15) << ":" << endl;
    outputFile << "----------------" << endl;
    outputFile << " --------------- --------------- ----------------"<<endl;
    for (map<string,pair<int, string>>::iterator it=table.begin(); it!=table.end(); ++it)
    {
        outputFile << "|" << (formatString(it->first,15)) << "|" << (formatNumber(it->second.first,15)) << "|" <<formatString(hexa(it->second.first,5),15)<< " |" << endl;
        outputFile << " --------------- --------------- ----------------"<<endl;
    }
}


void myPassOne::printLitTable(map<string, int > table,string msg)
{
    outputFile << endl << endl << formatString(msg,15) << ":" << endl;
    outputFile << "----------------" << endl;
    outputFile << " --------------- --------------- ----------------"<<endl;
    for (map<string,int>::iterator it=table.begin(); it!=table.end(); ++it)
    {
        outputFile << "|" << (formatString(it->first,15)) << "|" << (formatNumber(it->second,15)) << "|" <<formatString(hexa(it->second,5),15)<< " |" << endl;
        outputFile << " --------------- --------------- ----------------"<<endl;
    }
}


map<string, pair<int, string>> myPassOne::getSymbolTable()
{
    return symbolTable;
}
map<string, int> myPassOne::getLiteralTable()
{
    return literalTable;
}



string myPassOne::hexa(int decimal,int length)
{
    int i = length;
    if(decimal < 0 ) decimal = pow(16,7) + decimal ;
    string  hexaarray = "000000" ;
    int reminder = 0 ;
    while(decimal != 0)
    {
        reminder = decimal %16 ;
        if(reminder>9)
        {
            hexaarray[i] = 55 + reminder ;
        }
        else
        {
            hexaarray[i] = reminder + 48 ;

        }

        decimal /= 16;
        i--;
    }
    return hexaarray ;
}

bool myPassOne::exists(string st,map<string,int> theMap)
{

    map<string,int>::iterator itr = theMap.find(st);
    return itr != theMap.end();
}

int myPassOne::getLiteralLength(string literal)
{
    int length;
    if(regex_match(literal,regex("=C'.*'",regex_constants::icase)))
    {
        length = literal.length()-4;
    }
    else if (regex_match(literal,regex("=\\d+",regex_constants::icase)))
    {
        length = 3;
    }
    else
    {
        length = (literal.length()-4) / 2;
    }
    return length;

}

int myPassOne::getLength()
{
    return length;
}

