#include"assemblerHeaders.h"

bool passTwo::assemble(map<string,pair<int, string>> symbol_table, map<string,int> literal_table, int len )
{
    // registers table
    unordered_map<string,string> register_table = create_registers_table();
    // files
    ifstream file ("listingFile.txt");
    ofstream listfile ("listing file PassTwo.txt");
    ofstream errorfile ("error file PassTwo.txt");
    string out_file = "object file.txt";
    objfile.open(out_file.c_str(), ios::out);

    string line = "";
    bool base = false ;
    string baseAddr;
    parser parser;
    expressionHandler exprHandler(symbol_table);
    // to write T record
    int record_length = 0;
    string text_record;
    string objCode;
    string end_record = "";
    string starting_addr;  // starting address
    bool has_started = false; // true after write H record
    vector<string> modification_records;   //vectors for modification records
    bool objcode_exist = false; // write objcode in listing file or not
    bool successfulPassTwo = true;

    // rewrite program length
    string program_length = hexa(len);

    if(!file.is_open())
    {
        cout << "failed to open input file" << endl;
        return false;
    }

    while(!file.eof())
    {
        cout << "^^^^^^^^^^" << endl;
        getline (file,line);
        if(line == "End." || line=="")    //finish
        {
            break;
        }
        parser.handlePass2Line(line);
        cout << "******" << endl;
        string addr = parser.getAddress();
        if(parser.isComment())
        {
            listfile << line << endl;
            continue;
        }
        else if(parser.isErrLine())
        {
            continue;
        }

        if(!has_started)
        {
            string header_record = "H^";
            string symbol = "      ";
            if(CompareString(parser.getInsOrDir(), "start"))
            {
                starting_addr = addr;
                if(parser.hasSymbol())
                {
                    symbol = parser.getSymbol();
                    for(int i=symbol.length() ; i<6 ; i++)
                    {
                        symbol = symbol + " ";
                    }
                }
                symbol.append("^");
                header_record.append(symbol);
                header_record.append(addr);
                header_record.append("^");
                header_record.append(program_length);
                has_started = true;
                //write H record in file
                objfile << header_record << endl;
                listfile << line << endl;  // write in listing file
                continue;
            }// end if start directive
            else
            {
                header_record.append(symbol);
                header_record.append("^");
                header_record.append(addr);
                header_record.append("^");
                header_record.append(program_length);
                has_started = true;
                //write H record in file
                objfile << header_record << endl;
            }//end else if not have start directive
        }// end if not started ---> H record done

        if(parser.isLiteralLine())
        {
            string literal = parser.getLiteral();
            objCode = calculate_byte_word(literal.substr(1,literal.length()-1)); // remove =
            write_text_record(record_length, text_record, objCode, addr);
            listfile <<  line <<  "                              " << objCode << endl;
            continue;
        }// end if literal
        else if(CompareString(parser.getInsOrDir(),"end"))
        {
            // write E
            end_record = "E^";
            if(parser.hasOperands())
            {
                map<string,pair<int, string>>::iterator it = symbol_table.find(parser.getOperand());
                if(it==symbol_table.end())
                {
                    listfile << line << "******" <<endl;
                    listfile << "**** " << "Undifined Symbol" << endl;
                    errorfile << line << endl;
                    errorfile << "**** " << "Undifined Symbol" << endl;
                    successfulPassTwo = false;
                    continue;
                }
                string starting = hexa(it->second.first);
                end_record.append(starting);
            }
            else
            {
                end_record.append(starting_addr);
            }
            listfile << line << endl;
            continue;
        }// end if end directive
        else if(!parser.isDirective())
        {
            if(CompareString(parser.getInsOrDir(),"RSUB") || CompareString(parser.getInsOrDir(),"+RSUB"))
            {
                if(parser.getFormat() == 4)
                {
                    objCode = "4F000000";
                }
                else
                {
                    objCode = "4F0000";
                }
                // call T writer
                write_text_record(record_length, text_record, objCode, addr);
                objcode_exist = true;  //to write object code in listing file
            }//end else if RSUB
            else if(parser.getFormat() == 2)
            {
                string opcode = parser.getOperationCode();
                string operand = parser.getOperand();
                string reg;
                if(parser.isRegisterToRegister()) // r1,r2
                {
                    for(unsigned i=0 ; i<operand.length() ; i+=2)
                    {
                        reg = operand.substr(i,1);
                        unordered_map<string,string>::iterator it = register_table.find(reg);
                        if(it == register_table.end())
                        {
                            listfile << line << "******" <<endl;
                            listfile << "*****" << "Undifined Register" << endl;
                            errorfile << line << endl;
                            errorfile << "*****" << "Undifined Register" << endl;
                            successfulPassTwo = false;
                            continue;
                        }
                        opcode.append(it -> second);
                    }
                }// end if two registers
                else // one register or shift
                {
                    reg = operand.substr(0,1);
                    unordered_map<string,string>::iterator it = register_table.find(reg);
                    if(it == register_table.end())
                    {
                        listfile << line << "******" <<endl;
                        listfile << "*****" << "Undifined Register" << endl;
                        errorfile << line << endl;
                        errorfile << "*****" << "Undifined Register" << endl;
                        successfulPassTwo = false;
                        continue;
                    }
                    opcode.append(it -> second);
                    if(parser.isShiftIns())  // SHIFTL r,n
                    {
                        reg = operand.substr(2,operand.length()-2);
                        int n = strtoul(reg.c_str(), NULL,10); // get n
                        if(n > 0 && n < 17) // valid range
                        {
                            opcode.append(hexa(n-1).substr(5,1));
                        }
                        else
                        {
                            listfile << line << "******" <<endl;
                            listfile << "*****" << "Out of valid range" << endl;
                            errorfile << line << endl;
                            errorfile << "*****" << "Out of valid range" << endl;
                            successfulPassTwo = false;
                            continue;
                        }
                    } // end if shift
                    else // one register
                    {
                        opcode.append("0");
                    }
                }// end else one register or shift

                objCode = opcode;
                // call T writer
                write_text_record(record_length, text_record, objCode, addr);
                objcode_exist = true;  //to write object code in listing file

            }// end if format 2
            else
            {
                //claculate first byte
                int opcode = strtoul(parser.getOperationCode().c_str(), NULL, 16);
                if(parser.isImmediate())
                {
                    opcode += 1;   // flag i = 1
                }
                else if(parser.isIndirect())
                {
                    opcode += 2;   // flag n = 1
                }
                else  // not immediate not indirect
                {
                    opcode += 3;  // flag i=1 , n=1
                }
                string op = hexa(opcode);
                op = op.substr(4,2);
                string disp;
                string flags;

                //calculate disp or addr and flags
                string operand = parser.getOperand();
                string usedOperand = operand;
                if(operand.length() > 1)
                {
                    usedOperand = operand.substr(1,operand.length()-1);
                }
                exprHandler.evaluate(usedOperand,symbol_table);
                // immediate with decimal operand or absolute relative address
                if(parser.isImmediate() &&  (!isSymbolic(usedOperand) || (exprHandler.isValidExp() && exprHandler.isRelocatable()=="a")))
                {
                    if(exprHandler.isValidExp()) // expression operand
                    {
                        disp = hexa(exprHandler.getFinalResult());
                    }
                    else // decimal operand
                    {
                        disp = hexa(strtoul(usedOperand.c_str(), NULL, 10));
                    }
                    if(parser.getFormat() == 4)
                    {
                        // 5 hex dicimal
                        disp = disp.substr(1,5);
                        flags = "1";
                    }// end if format 4 in case decimal operand in immediate
                    else
                    {
                        // format 3
                        disp = disp.substr(3,3);
                        flags = "0";
                    }// end if format 3

                }// end if immediate with decimal operand
                else // not immediate
                {
                    int temp = 0; // to calculate flags
                    if(parser.isIndirect())
                    {
                        operand = usedOperand; // remove '@'
                    }
                    else if(parser.isImmediate())
                    {
                        operand = usedOperand; // remove '#'
                    }
                    if(parser.isIndexing())
                    {
                        operand = operand.substr(0,operand.length()-2); // remove ',x'
                        temp += 8; // flag x = 1
                    }
                    if(parser.getFormat() == 4)
                    {
                        temp += 1; // flag e = 1
                        if(parser.isLiteral())  //search in literal table
                        {
                            map<string,int>::iterator it = literal_table.find(operand);
                            if(it == literal_table.end())
                            {
                                listfile << line << "******" <<endl;
                                listfile << "**** " << "Undifined Literal" << endl;
                                errorfile << line << endl;
                                errorfile << "**** " << "Undifined Literal" << endl;
                                successfulPassTwo = false;
                                continue;
                            }
                            operand = hexa(it -> second);

                        }// end if literal
                        else  // search in symbol table
                        {
                            exprHandler.evaluate(operand,symbol_table);
                            if(exprHandler.isValidExp())
                            {
                                operand = hexa(exprHandler.getFinalResult());
                            }
                            else // error
                            {
                                listfile << line << "******" << endl;
                                listfile << "**** " << "Undifined Symbols" << endl;
                                errorfile << line << endl;
                                errorfile << "**** " << "Undifined Symbols" << endl;
                                successfulPassTwo = false;
                                continue;
                            }
                        } // end if symbol
                        disp = operand.substr(1,5);
                        flags =hexa(temp);
                        flags = flags.at(flags.length()-1);
                        if(!parser.isImmediate() && exprHandler.isRelocatable() == "r")  //add modification record
                        {
                            modification_records.push_back(modification(addr, true));
                        }
                    }
                    else // not extended absolute operand or use pc relative addressing
                    {
                        // check if absolute
                        exprHandler.evaluate(operand,symbol_table);
                        if(exprHandler.isValidExp() && exprHandler.isRelocatable() == "a")
                        {
                            disp = hexa(exprHandler.getFinalResult()).substr(3,3);
                            flags = "0";
                        }
                        else
                        {
                            int pc = strtoul(addr.c_str(), NULL, 16) + parser.getFormat();
                            int TA; // target address
                            if(parser.isLiteral())  // search in literal table
                            {
                                map<string,int>::iterator it = literal_table.find(operand);
                                if(it == literal_table.end())
                                {
                                    listfile << line << "******" << endl;
                                    listfile << "**** " << "Undifined Literal" << endl;
                                    errorfile << line << endl;
                                    errorfile << "**** " << "Undifined Literal" << endl;
                                    successfulPassTwo = false;
                                    continue;
                                }
                                TA = it -> second;
                            }
                            else // not literal search in symbol table
                            {
                                exprHandler.evaluate(operand,symbol_table);
                                if(exprHandler.isValidExp())
                                {
                                    TA = exprHandler.getFinalResult();
                                }
                                else // error
                                {
                                    listfile << line << "******" << endl;
                                    listfile << "**** " << "Undifined Symbols" << endl;
                                    errorfile << line << endl;
                                    errorfile << "**** " << "Undifined Symbols" << endl;
                                    successfulPassTwo = false;
                                    continue;
                                }

                            }
                            int disp_integer = TA - pc;
                            if(disp_integer <= 2047 && disp_integer >= -2048)
                            {
                                temp += 2;
                                flags = hexa(temp);
                                flags = flags.at(flags.length()-1);
                            }
                            else
                            {
                                // use base relative addressing
                                if(base)
                                {
                                    int b = strtoul(baseAddr.c_str(), NULL, 16);
                                    disp_integer = TA - b;
                                    temp += 4;
                                    flags = hexa(temp);
                                    flags = flags.at(flags.length()-1);
                                }// end if base
                                else
                                {
                                    listfile << line << "******" << endl;
                                    listfile << "**** " << "can't use PC relative Or Base relative" << endl;
                                    errorfile << line << endl;
                                    errorfile << "**** " << "can't use PC relative Or Base relative" << endl;
                                    successfulPassTwo = false;
                                    continue;
                                }
                            }// end else if base
                            disp = hexa(disp_integer);
                            disp = disp.substr(3,3); // format 3
                        } // end use relative addressing
                    }//end else if not extended using p relative addressing

                }// end else if not immediate with dicimal operand
                objCode ="";
                objCode = op.append(flags);
                objCode.append(disp);

                // call T writer
                write_text_record(record_length, text_record, objCode, addr);
                objcode_exist = true;   //to write object code in listing file

            } // end else if not directive and not format 2
        }// end if not directive
        else // directives --> base , nobase , byte , word , resb , resw , LTORG , ORG , EQU
        {
            if(CompareString(parser.getInsOrDir(),"LTORG"))
            {
                listfile << line << endl;
                continue;
            }
            else if(CompareString(parser.getInsOrDir(),"ORG"))
            {
                listfile << line << endl;
                continue;
            }
            else if(CompareString(parser.getInsOrDir(),"EQU"))
            {
                listfile << line << endl;
                continue;
            }
            else if(CompareString(parser.getInsOrDir(),"base"))
            {
                base = true;
                map<string,pair<int, string>>::iterator it = symbol_table.find(parser.getOperand());
                baseAddr = hexa(it -> second.first);

            }//end if base
            else if(CompareString(parser.getInsOrDir(),"nobase"))
            {
                base = false;
            }//end if no base
            else if(CompareString(parser.getInsOrDir(),"byte"))
            {
                string operand =parser.getOperand();
                objCode = calculate_byte_word(operand);
                // call T writer
                write_text_record(record_length, text_record, objCode, addr);
                objcode_exist = true;   //to write object code in listing file
            }//end else if byte
            else if (CompareString(parser.getInsOrDir(),"word"))
            {
                string printedObjcode = "";
                string number;
                stringstream operandStream(parser.getOperand());
                while (getline(operandStream,number, ','))
                {
                    exprHandler.evaluate(number,symbol_table);
                    if(exprHandler.isValidExp())
                    {
                        objCode = hexa(exprHandler.getFinalResult());
                        printedObjcode.append(objCode);
                        printedObjcode.append("^");
                    }
                    else // error
                    {
                        listfile << line << "******" << endl;
                        listfile << "**** " << "Undifined Symbols" << endl;
                        errorfile << line << endl;
                        errorfile << "**** " << "Undifined Symbols" << endl;
                        successfulPassTwo = false;
                        continue;
                    }
                    write_text_record(record_length, text_record, objCode, addr);

                }
                objCode = printedObjcode;
                objcode_exist = true;   //to write object code in listing file

            }//end else if word
            else if(CompareString(parser.getInsOrDir(),"resb") || CompareString(parser.getInsOrDir(),"resw"))
            {
                // call T to break
                if(record_length != 0)   // write previous text record
                {
                    string len = hexa(record_length/2);
                    len = len.substr(4,len.length()-4);

                    objfile << len << "^" << text_record << endl;
                }
                record_length = 0;
                text_record = "";

            }//end else if reserve directive

        }//end else if directive

        if(objcode_exist)
        {
            //write listing file
            listfile <<  line << objCode << endl;
        }
        else
        {
            listfile << line << endl;
        }
        objcode_exist = false; // reset

    } // end while

    // end listing file of passOne --> write E record
    // write remain T record
    if(record_length != 0)
    {
        string len = hexa(record_length/2);
        len = len.substr(4,2);
        objfile << len << "^" << text_record << endl;
    }
    // write modificatio records
    for(string record : modification_records)
    {
        objfile << record << endl;
    }
    // write E record
    objfile << end_record <<endl;

    // close files
    objfile.close();
    listfile.close();
    file.close();

    return successfulPassTwo;
}



bool passTwo::CompareString(std::string s1,std::string s2)
{
    // if string ==null
    if(s1.empty() || s1.length()>s2.length()|| s1.length()>s2.length())
    {
        return false;
    }
    else
    {
        for(unsigned i=0; i<s1.length() ; i++)
        {
            char a =tolower(s1.at(i));
            char b =tolower(s2.at(i));
            if(a>b || a<b)
            {
                return false;
            }
        }
        return true;
    }
}
string passTwo::hexa(int decimal)
{
    int i=5;
    if(decimal < 0 ) decimal = pow(16,7) + decimal ;
    string  hexaarray = "000000";
    int reminder = 0 ;
    while(decimal != 0)
    {
        reminder = decimal %16 ;
        if(reminder>9)
        {
            if(reminder == 10)
            {
                hexaarray[i] = 65;
            }
            else if(reminder ==11)
            {
                hexaarray[i]=66;
            }
            else if(reminder ==12)
            {
                hexaarray[i]=67;
            }
            else if(reminder ==13)
            {
                hexaarray[i]=68;
            }
            else if(reminder ==14)
            {
                hexaarray[i]=69;
            }
            else if(reminder ==15)
            {
                hexaarray[i]=70;
            }
        }
        else
        {
            hexaarray[i] = reminder + 48 ;

        }

        decimal /= 16;
        i--;
    }

    return hexaarray ;

}

unordered_map<string, string> passTwo::create_registers_table()
{
    unordered_map<string,string> registers_table;
    registers_table.insert(make_pair("A","0"));
    registers_table.insert(make_pair("X","1"));
    registers_table.insert(make_pair("L","2"));
    registers_table.insert(make_pair("B","3"));
    registers_table.insert(make_pair("S","4"));
    registers_table.insert(make_pair("T","5"));
    registers_table.insert(make_pair("F","6"));
    registers_table.insert(make_pair("PC","8"));
    registers_table.insert(make_pair("SW","9"));
    return registers_table;
}

void passTwo::write_text_record(int &record_length, string &text_record, string objCode, string addr)
{
    //call T record writer
    if(record_length==0 || (record_length + objCode.length()) > 60)
    {
        //if > 60 write previous text record first
        if((record_length + objCode.length()) > 60)
        {
            string len = hexa(record_length/2);
            len = len.substr(4,2);
            objfile << len << "^" << text_record << endl;

        }
        //initial new text record
        objfile << "T^" << addr << "^";
        record_length = objCode.length();
        text_record = objCode;
        text_record.append("^");
    }
    else // write in the same text record
    {
        text_record.append(objCode);
        text_record.append("^");
        record_length += objCode.length();
    }

}


string passTwo::modification(string address, bool extended)
{
    string modi_record ="M^";
    if(extended)  // format 4
    {
        int decimal = strtoul(address.c_str(), NULL, 16);
        decimal =decimal +1;
        string hexa_string = hexa(decimal);
        modi_record.append(hexa_string);
        modi_record.append("^");
        string length = "05";
        modi_record.append(length);
    }
    else
    {
        // another need modification
    }

    return modi_record;
}

string passTwo::calculate_byte_word(string operand)
{
    string objCode;
    if(operand.at(0)== 'c'|| operand.at(0)== 'C')
    {
        objCode = "";
        for(unsigned i =2 ; i<operand.length()-1 ; i++)
        {
            // convert to ASCII --> hexa
            int ch = operand.at(i);
            string dummy = hexa(ch);
            objCode.append(dummy.substr(4,2));
        }
    }//end if byte C
    else if(operand.at(0)== 'X'|| operand.at(0)== 'x')
    {
        objCode = operand.substr(2,operand.length()-3);
    }//end else if byte X
    else  // if decimal
    {
        objCode = hexa(strtoul(operand.c_str(), NULL, 10));
    } //end

    return objCode;
}

bool passTwo::isSymbolic(string operand)
{
    if(operand.at(0)=='-')
    {
        if(operand.at(1) >= '0' && operand.at(1) <= '9' )
            return false;
    }
    else if(operand.at(0) >= '0' && operand.at(0) <= '9' )
    {
        return false;
    }

    return true;
}
