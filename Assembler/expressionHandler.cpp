#include "assemblerHeaders.h"

bool isValid=true;
int relocationCounting=0;
element finalResult;
map<string, pair<int, string>> symbol_table ;

using namespace std;

expressionHandler::expressionHandler(std::map<string, pair<int, string>> &sym)
{
    symbol_table = sym ;
}

int expressionHandler::getWeight(string sign)
{
    char ch= sign.at(0);
    switch (ch)
    {
    case '/':
    case '*':
        return 2;
    case '+':
    case '-':
        return 1;
    default :
        return 0;
    }
}

element expressionHandler::getValue(string st)
{
    element newElement;
    if(regex_match(st, std::regex("^0[0-9A-F]+$") ))
    {
        newElement.isInteger=true;
        newElement.isRelocatable=false;
        unsigned int x;
        std::stringstream ss;
        ss << std::hex << st;
        ss >> x;
        newElement.value=x;
        newElement.st=st;

    }

    else if(regex_match(st, std::regex("^-?\\d+$") ))
    {
        newElement.isInteger=true;
        newElement.isRelocatable=false;
        newElement.value=atoi(st.c_str());
        newElement.st=st;
    }

    else if(regex_match(st, std::regex("^\\*$") ))
    {
        newElement.isInteger=true;
        newElement.isRelocatable=true;
//        int val= locationCounter
//        newElement.value=atoi(val);
//        string locCounter;
//        ostringstream convert;
//        convert << val;
//        resultString = convert.str();
//        newElement.st(resultString);
    }
    else
    {
        map<string,pair<int, string>>::iterator itr = symbol_table .find(st);
        if(itr != symbol_table .end())
        {

            newElement.value=itr->second.first;
            string stRelocatable =itr->second.second;
            if(regex_match(stRelocatable, std::regex("^r$",regex_constants::icase)))

            {
                newElement.isRelocatable= true;
            }
            else
                newElement.isRelocatable= false;

        }
        else
        {
            //throw std::invalid_argument( "received invalid Calculation" );
            isValid=false;
        }
    }
    return newElement;
}

element expressionHandler::calc(element ele1,element ele2,string sign)
{
    bool validCalu=true;
    element result ;
    int value;
    char signChar=sign.at(0);
    result.isRelocatable=false;
    if(signChar=='+')
    {
        if(ele1.isRelocatable &&ele2.isRelocatable )
        {
            result.isRelocatable=true;
            relocationCounting=relocationCounting+2;
        }
        if((!ele1.isRelocatable &&ele2.isRelocatable) || (ele1.isRelocatable &&! ele2.isRelocatable) )
        {
            relocationCounting=relocationCounting+1;
            result.isRelocatable=true;
        }

        value=ele2.value+ele1.value;

    }
    else if(signChar=='-')
    {
        value=ele2.value-ele1.value;
        if(ele1.isRelocatable &&ele2.isRelocatable )
        {
            result.isRelocatable=false;
        }
        else if(!ele1.isRelocatable &&ele2.isRelocatable )
        {
            relocationCounting=relocationCounting+1;
            result.isRelocatable=true;
        }
        else if(ele1.isRelocatable && !ele2.isRelocatable )
        {
            relocationCounting=relocationCounting-1;
            result.isRelocatable=true;
        }
    }
    else if(signChar=='*')
    {
        if(ele1.isRelocatable ||ele2.isRelocatable )
        {
            validCalu=false;
            //error :trying to multiply relocatable
        }
        value=ele2.value*ele1.value;
    }
    else if(signChar=='/')
    {
        if(ele1.isRelocatable || ele2.isRelocatable )
        {
            //error :trying to multiply relocatable
            validCalu=false;
            throw std::invalid_argument( "received invalid Calculation" );
        }

        value=ele2.value/ele1.value;
    }

    if(!validCalu)
    {
        isValid=false;
        throw std::invalid_argument( "received invalid Calculation" );
    }
    string resultString;
    ostringstream convert;
    convert << value;
    resultString = convert.str();

    result.st=resultString;
    result.isInteger=true;
    result.value=value;
    return result;
}

void expressionHandler::evaluate(string str ,map<string, pair<int, string>> sym)
{

    symbol_table=sym;
    isValid=true;
    relocationCounting=0;
    std::stack<string> symbolsStack;
    std::stack<string> postFixStack;
    std::stack<element> postFixElements;
    string singleElement;
    std::regex rgx("^(-?\\d+|[a-zA-Z]\\w*|0[A-F][0-9A-F]+)");
    std::regex rgx2("^(\\()");
    std::regex rgx4("^(\\))");
    std::regex rgx3("^([+|-|*|/])");

    std::smatch match;
    bool preceededBySign=true;

//
//    if(regex_match(str.at(0), std::regex("#") ))
//    {
//        isImidiate=true;
//        str = str.replace(0,1, "");
//    }
//    if(regex_match(str.at(0), std::regex("@") ))
//    {
//        isIndirect=true;
//        str = str.replace(0,1, "");
//    }

    while(str.length()>0 && isValid)
    {
        if (std::regex_search(str, match, rgx)&&preceededBySign) // number or symbol
        {
            singleElement=match[1];
            try
            {
                element ele=getValue(singleElement);
                postFixElements.push(ele);
            }
            catch(exception e)
            {
                isValid=false;
                break;
            }
            str = str.replace(str.begin(),str.begin() +singleElement.length(), "");
             preceededBySign=false;
        }
        else if(std::regex_search(str, match, rgx2)) //  ---- ( ------
        {
            singleElement=match[1];
            symbolsStack.push(singleElement);
            str = str.replace(str.begin(),str.begin() +singleElement.length(), "");
             preceededBySign=true;
        }
        else if(std::regex_search(str, match, rgx3)) // sign * - + /
        {
            singleElement=match[1];
            if(!symbolsStack.empty())
            {
                string pp=symbolsStack.top();
                if(std::regex_match(pp, rgx3) && getWeight(singleElement)<= getWeight(pp))
                {
                    element ele1=postFixElements.top();
                    postFixElements.pop();
                    element ele2=postFixElements.top();
                    postFixElements.pop();
                    try
                    {
                        element result=calc(ele1,ele2,pp);
                        postFixElements.push(result);
                    }
                    catch( const std::invalid_argument& e )
                    {
                        isValid=false;
                        break;
                    }
                    //cout<< "calculated" <<result.value<<"\n";
                    symbolsStack.pop();
                }

            }
            symbolsStack.push(singleElement);
            str = str.replace(str.begin(),str.begin() +singleElement.length(), "");
            preceededBySign=true;
        }
        else if(std::regex_search(str, match, rgx4)) // ----------- ) ----------
        {
            singleElement=match[1];
            string kk = symbolsStack.top();
            while(!std::regex_match(kk, rgx2))
            {
                symbolsStack.pop();
                element ele1=postFixElements.top();
                postFixElements.pop();
                element ele2=postFixElements.top();
                postFixElements.pop();
                try
                {
                    element result=calc(ele1,ele2,kk);
                    postFixElements.push(result);
                }
                catch( const std::invalid_argument& e )
                {
                    isValid=false;
                    break;
                }


                kk = symbolsStack.top();
                if(symbolsStack.empty())
                {
                    isValid=false;
                    break;
                }
            }
            symbolsStack.pop(); // removing '( '
            str = str.replace(str.begin(),str.begin() +singleElement.length(), "");
            preceededBySign=false;
        }
        else
        {
            isValid=false;
            break;
        }
    }
    // "------------ empty the stack ---------------------------"
    while(!symbolsStack.empty() && isValid)
    {
        string lk =symbolsStack.top();
        symbolsStack.pop();
        element ele1=postFixElements.top();
        postFixElements.pop();
        element ele2=postFixElements.top();
        postFixElements.pop();

        element result=calc(ele1,ele2,lk);
        postFixElements.push(result);
    }

    if(isValid)
    {
        finalResult=postFixElements.top();
    }
//    cout<<"---------------------------- \n";
//    cout<< isValid;
//    cout<<"---------------------------- \n";
//    cout<<finalResult.isRelocatable;
//    cout<<"---------------------------- \n";
//    cout<<finalResult.value;
//    cout<<"---------------------------- \n";

}

bool expressionHandler::isValidExp()
{
    if(relocationCounting==0||relocationCounting==1||relocationCounting==-1)
    {
        return isValid;

    }
   // cout<<false;
    return false;
}

string  expressionHandler::isRelocatable()
{
    if (finalResult.isRelocatable){
        return "r";
    }
    return "a";
}

int expressionHandler::getFinalResult()
{
    return finalResult.value;
}
