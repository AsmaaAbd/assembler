#ifndef HEADERS_H_INCLUDED
#define HEADERS_H_INCLUDED
#endif // HEADERS_H_INCLUDED


#ifndef MAIN_H_INCLUDED
#define MAIN_H_INCLUDED
#endif // MAIN_H_INCLUDED
#include <iostream>
#include <string>
#include<regex>
#include <iterator>
#include <map>
#include <fstream>
#include <unordered_map>
#include<stdio.h>
#include <bits/stdc++.h>
#include <string.h>
#include <math.h>
#include <ctype.h>


using namespace std;

struct element
{
    string st ;
    int value;
    bool isRelocatable;
    bool isSign=false;
    bool isInteger;
};


class expressionHandler
{
public:
    expressionHandler(map<string , pair<int , string>> &sym);
    void evaluate(string str,map<string, pair<int, string>> sym);
    bool isValidExp();
    string  isRelocatable();
    int getFinalResult();


protected:

private:
    int relocationCounting;
    element finalResult;
    bool isValid;
    element getValue(string st);
    int getWeight(string sign);
    element calc(element ele1,element ele2,string sign);
    map<string , pair<int , string>> symbol_table ;
};


class directiveReader
{
private:
    unordered_map<string,string> directiveTable; //DIRTABLE
public:
    void readDirective();
    unordered_map<string,string> getDirTable();
};



class operation
{
private:
    string mnemonic; //mnemonic operation
    string opCode; //opcode
    int format; //format 2 or 3
    bool hasOperand; //has operand
    bool memoryOper; // memory operand is valid
    int registerOper; // register operand is valid
public:
    operation(string mnem,string opCd,int frmt, bool hasOp, bool memory, int regit);    // constructor
    string getMnemonic();//returns the operator
    string getOpCode();//returns the operation code
    int getFormat();//format 2 or 3
    bool hasOperands();//if the operation takes operands
    bool hasMemoryOper(); // if valid memory as operand
    int getRegisterOper(); // if valid registers as operand


};




class readFile
{
private:
    unordered_map<string,operation> table; //OPTABLE
public:
    void readTable();
    unordered_map<string,operation> getTable();
};


class parser
{

private:
    readFile reader;
    directiveReader dReader;
    std::regex instructionWithNoSymbol;
    std::regex instructionWithNoSymbolAndCommnet;
    std::regex instructionWithSymbol;
    std::regex instructionWithSymbolAndComment;
    std::regex RSUBWithSymbol;
    std::regex commentRegex;
    std::regex directiveWithSymbolAndOperand;
    std::regex directiveWithNoSymbolAndOperand;
    std::regex directiveWithNoSymbolNorOperand;
    std::string symbol;
    std::string operationOrDirective;
    std::string operand;
    std::string operationCode;
    std::string warning;
    std::string err;
    std::string comment;
    std::string address;
    string literal;
    int lineNumber;
    bool isErrorLine;
    bool isComm;
    bool isDir;
    bool hasSym;
    bool hasOprnds;
    bool isVld;
    bool hasWrnng;
    bool hasErr;
    bool isImm;
    bool isIndir;
    bool isIndex;
    bool isLit;
    bool isSymbolic;
    bool isRegToReg;
    bool isShift;
    bool isLitLine;
    bool isExp;
    int format;
    bool isBlank(string line);
    bool matchComment(std::string line);
    bool matchesOperation(string line);
    bool matchRSUB(string line);
    bool matchInstructionWithNoSymbol(std::string line);
    bool matchInstructionWithSymbol(std::string line);
    bool isValidInstructionOperand(string operand,operation op);
    bool matchesDirective(string line);
    bool matchesDirectiveWithSymbolAndOperand(string line);
    bool matchesDirectiveWithNoSymbolAndOperand(string line);
    bool matchesDirectiveWithNoSymbolNorOperand(string line);
    bool isAcceptedDirectiveOperand(string oprnd,string oprtn);
    void initialize();






public:
    parser();
    void handleLine(std::string line);
    bool isComment();
    bool hasSymbol();
    bool isDirective();
    bool isValid();
    bool hasOperands();
    bool hasWarning();
    bool hasError();
    bool isImmediate();
    bool isIndirect();
    bool isIndexing();
    bool isErrLine();
    bool isRegisterToRegister();
    bool isShiftIns();
    bool hasSymbolicOperand();
    bool isLiteral();
    bool isLiteralLine();
    bool isExpression();
    int getLineNumber();
    int getFormat();
    string getLiteral();
    string getAddress();
    std::string getOperationCode();
    std::string getSymbol();
    std::string getInsOrDir();
    std::string getOperand();
    std::string getWarning();
    std::string getError();
    std::string getComment();
    void toUpper(string &instructionOrDirective);
    void handlePass2Line(string line);
    bool matchesDirectiveOrInstruction(string labelOrSymbol);

};


class pass1
{
public:

    string input;
    ofstream ofile ;
    int line ;

    int getNumber(std::string s1);
    int getLength (string operand) ;
    bool assemble(string path) ;
    bool checkAddressAvalability(std::string s);
    bool CompareString(std::string s1,std::string s2);
    bool searchOptable(string s);
    void print_line(parser parser,string hex, bool isComment, bool isError, bool is_address, string error) ;
    string getMnemonic(string s);
    string hexa(int decimal) ;
    void printSymTable(std::unordered_map<string, int > symbolTable);
    string formatSymbol (string line);
    string formatAddress(int number);
    unordered_map<string, int> get_symptab();
    int get_program_length();

};



class myPassOne
{
private :

    ofstream outputFile ;
    map<string, pair<int , string>> symbolTable;
    map <string,int>literalTable;
    void printTable(map<string, pair<int , string> > table,string msg);
    void printLitTable(map<string, int > table,string msg) ;
    string formatNumber(int number,int length);
    string formatString (string line,int length);
    string hexa(int decimal,int length);
    int getLiteralLength(string literal);
    int length;
    bool exists(string st,map<string,int> theMap);
public:
    bool assemble(string path);
    map<string, pair<int , string>> getSymbolTable();
    map<string,int> getLiteralTable();
    int getLength();
};



class passTwo
{
public:
    bool assemble(map<string,pair<int , string>> symbolTable,map<string,int> literalTable, int program_length);
private:
    ofstream objfile;
    bool CompareString(std::string s1,std::string s2);
    string hexa(int decimal) ;
    string getObjectCode(string address,string target_Add,bool base,parser parser);
    unordered_map<string, string> create_registers_table();
    void write_text_record(int &record_length, string &text_record, string objCode, string addr);
    string modification(string address,bool extended);
    string calculate_byte_word(string operand);
    bool isSymbolic(string operand);
};
