#include "assemblerHeaders.h"

//class for read OPTABLE from external file

unordered_map<string,operation> readFile::getTable()
    {
        return table;
    }
void readFile::readTable()
{
    string line;
    ifstream myfile ("appendix.txt");
    if(myfile.is_open())
    {
        getline (myfile,line); // first line not needed
        while(! myfile.eof() )
        {
            getline (myfile,line); // read line from file
            string mnemonic, opcode;
            int format;
            bool operand;
            bool memoryOper;
            int registerOper;
            stringstream(line) >> mnemonic >> operand >> format >> opcode >> memoryOper >> registerOper;
            operation dummy (mnemonic, opcode, format, operand ,memoryOper , registerOper);    //create new object from operation
            table.insert(make_pair( mnemonic,  dummy)); //insert in OPTABLE

        }
        myfile.close();
    }
    else cout << "Unable to open file";


}
